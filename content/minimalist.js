// Providing shortcuts for many otherwise lengthy XPCOM interfaces and constants
const PREF_STRING          = Components.interfaces.nsIPrefBranch.PREF_STRING
const nsIPrefBranch        = Components.interfaces.nsIPrefBranch
const nsIPromptService     = Components.interfaces.nsIPromptService
const nsIURI               = Components.interfaces.nsIURI

// Generic constants
const settings             = Components.classes['@zelgadis.jp/minimalist;1'].getService().wrappedJSObject
const preferenceManager    = Components.classes['@mozilla.org/preferences-service;1'].getService(nsIPrefBranch)
const promptService        = Components.classes['@mozilla.org/embedcomp/prompt-service;1'].getService(nsIPromptService)
const sourceURL            = Components.classes['@mozilla.org/network/standard-url;1'].getService(nsIURI)
const destinationURL       = sourceURL.clone()

// Various regular expressions. They are automatically compiled once on startup due to the literal notation
const extractRefreshURL    = /url=(.+)/i


/* Purpose: Gets called for every new document and does everything we can't do within the XPCOM code
 */
const onDocumentLoaded = function(event) {
	// This sub-function scans for meta refresh tags and immediately follows their target
	if (settings.skipRefresh) {
		var metaTags = event.target.getElementsByTagName('meta')

		for each (var metaTag in metaTags) {
			if (metaTag.httpEquiv.toLowerCase() == 'refresh') {
				if (match = extractRefreshURL.exec(metaTag.content)) {
					sourceURL.spec      = event.target.location.href
					destinationURL.spec = srcURL.resolve(match[1])

					// Make sure we're not loading this document again, else we'd end up in an infinite loop
					if (!sourceURL.equals(destinationURL)) {
						event.target.location.replace(dstURL.spec) } } } } } }

getBrowser().addEventListener('DOMContentLoaded', onDocumentLoaded, false)


/* Purpose: Set the extension level and the button image
 */
const initializeButton = function(self, newLevel) {
	preferenceManager.setIntPref('extensions.minimalist.level', newLevel)
	self.src = 'chrome://minimalist/content/L' + newLevel + '.png' }


/* Purpose: Gets called every time the user clicks on the statusbar panel and once on startup.
 *          It acts differently depending on which mouse button was clicked
 */
const onButtonClicked = function(self, button, behavior) {
	switch (button) {
		// Filter level button
		case 0:
			switch (behavior) {
				case 4:
					// This is used once at startup. It doesn't change the filter level, but sets the icon
					initializeButton(self, settings.level)
					break

				case 0:
					// Left mouse button was clicked. Toggle between filter level 0 and 3
					initializeButton(self, (settings.level == 3) ? 0 : 3)
					break

				case 2:
					// Right mouse button was clicked. Show a list of blocked sites for this tab
					var popupMenu = document.getElementById('minimalist-filter-popup')

					// Remove all previous entries
					while (popupMenu.lastChild) {
						popupMenu.removeChild(popupMenu.lastChild) }

					// <\Skip redirection delays>: Insert into menu list
					var settingsItem1  = document.createElement('menuitem')
					settingsItem1.onclick = onChangeSetting
					settingsItem1.setAttribute('label', 'Skip redirection delays')
					settingsItem1.setAttribute('closemenu', 'none')
					settingsItem1.setAttribute('type', 'checkbox')
					settingsItem1.setAttribute('checked', settings.skipRefresh)

					// <\Global embed whitelist>: Insert into menu list
					var settingsMenu1  = document.createElement('menu')
					var settingsPopup1 = document.createElement('menupopup')
					settingsMenu1.setAttribute('label', 'Global embed whitelist')

					// <\Whitelisted domains>: Insert into menu list
					var settingsMenu2  = document.createElement('menu')
					var settingsPopup2 = document.createElement('menupopup')
					settingsMenu2.setAttribute('label', 'Whitelisted domains')

					// <\Blocked resources>: Insert into menu list
					var settingsMenu3  = document.createElement('menu')
					var settingsPopup3 = document.createElement('menupopup')
					settingsMenu3.setAttribute('label', 'Blocked resources')

					// <\Level selection>: Insert into menu list
					var settingsMenu4  = document.createElement('menu')
					var settingsPopup4 = document.createElement('menupopup')
					settingsMenu4.setAttribute('label', 'Filter Level')
					for (var i = 0; i < 4; i++) {
						var menuItem = document.createElement('menuitem')
						menuItem.setAttribute('label', 'Level ' + i)
						menuItem.onclick = function(new_level) {
							return function () { initializeButton(self, new_level) } } (i)
						settingsPopup4.appendChild(menuItem) }

					// <\Blocked resources>: Prepare deeper menu level
					var currentWindow = gBrowser.selectedBrowser.contentWindow.location.href
					if (currentWindow in settings.blockedSites) {
						var destinationHosts = settings.blockedSites[currentWindow]
						for (var destinationHost in destinationHosts) {
							// <\Blocked resources\[hosts]>: Build contents
							var menuForHost = document.createElement('menu')
							menuForHost.setAttribute('label', destinationHost)

							// <\Blocked resources\[hosts]>: Prepare deeper menu level
							var menuForPaths     = document.createElement('menupopup')
							var destinationPaths = destinationHosts[destinationHost]
							for (var destinationPath in destinationPaths) {
								var blockedResource = destinationPaths[destinationPath]

								// <\Blocked resources\[hosts]\[paths]>: Build contents
								var pathMenuItem = document.createElement('menuitem')
								pathMenuItem.onclick = onChangeWhitelistEntry
								pathMenuItem.setUserData(   'responsibleTag',    blockedResource.responsibleTag, null)
								pathMenuItem.setUserData(     'sourceDomain',      blockedResource.sourceDomain, null)
								pathMenuItem.setUserData('destinationDomain', blockedResource.destinationDomain, null)
								pathMenuItem.setAttribute('label',
									'[L' + blockedResource.filterLevel + ':' + blockedResource.responsibleTag + '] ' +
									destinationPath)

								menuForPaths.appendChild(pathMenuItem) }
							menuForHost.appendChild(menuForPaths)
							settingsPopup3.appendChild(menuForHost) } }

					// <\Blocked resources\Empty>: Insert into menu list (if necessary)
					if (!settingsPopup3.firstChild) {
						var emptyMenuItem = document.createElement('menuitem')
						emptyMenuItem.setAttribute('label', 'Empty')
						emptyMenuItem.setAttribute('disabled', true)
						settingsPopup3.appendChild(emptyMenuItem) }

					// <\Global embed whitelist>: Prepare deeper menu level
					var settingPath ='extensions.minimalist.embed'
					var whiteList   = preferenceManager.getCharPref(settingPath).split(',')
					if (whiteList.length > 1 || whiteList[0] != '') {
						// <\Global embed whitelist\[domains]>: Build contents
						for each (destinationDomain in whiteList) {
							var listMenuItem = document.createElement('menuitem')
							listMenuItem.onclick = onChangeWhitelistEntry
							listMenuItem.setUserData('domainList', whiteList, null)
							listMenuItem.setUserData('settingPath', settingPath, null)
							listMenuItem.setAttribute('label', destinationDomain)
							listMenuItem.setAttribute('value', '<embeds>')
							settingsPopup1.appendChild(listMenuItem) } }
					else {
						// <\Global embed whitelist\Empty>: Insert into menu list
						var listMenuItem = document.createElement('menuitem')
						listMenuItem.setAttribute('label', 'Empty')
						listMenuItem.setAttribute('disabled', true)
						settingsPopup1.appendChild(listMenuItem) }

					// <\Whitelisted domains>: Prepare deeper menu level
					var currentDomain = settings.getDomainName(currentWindow)
					    settingPath   = 'extensions.minimalist.whitelist.' + currentDomain
					if (preferenceManager.getPrefType(settingPath) == PREF_STRING) {
						whiteList = preferenceManager.getCharPref(settingPath).split(',')

						// <\Whitelisted domains\[domains]>: Build contents
						if (whiteList.length > 1 || whiteList[0] != '') {
							for each (destinationDomain in whiteList) {
								var listMenuItem = document.createElement('menuitem')
								listMenuItem.onclick = onChangeWhitelistEntry
								listMenuItem.setUserData('domainList', whiteList, null)
								listMenuItem.setUserData('settingPath', settingPath, null)
								listMenuItem.setAttribute('label', destinationDomain)
								listMenuItem.setAttribute('value', currentDomain)
								settingsPopup2.appendChild(listMenuItem) } } }

					// <\Whitelisted domains\Empty>: Insert into menu list (if necessary)
					if (!settingsPopup2.firstChild) {
						var listMenuItem = document.createElement('menuitem')
						listMenuItem.setAttribute('label', 'Empty')
						listMenuItem.setAttribute('disabled', true)
						settingsPopup2.appendChild(listMenuItem) }

					// Put everything together and attach it to the XUL document popup node
					settingsMenu1.appendChild(settingsPopup1)
					settingsMenu2.appendChild(settingsPopup2)
					settingsMenu3.appendChild(settingsPopup3)
					settingsMenu4.appendChild(settingsPopup4)
					popupMenu.appendChild(settingsItem1)
					popupMenu.appendChild(settingsMenu1)
					popupMenu.appendChild(settingsMenu2)
					popupMenu.appendChild(settingsMenu3)
					popupMenu.appendChild(settingsMenu4)
					self.parentNode.appendChild(popupMenu)

					// Display the popup
					popupMenu.openPopup(self, 'before_end', 0, 0, true, false)
					break }
			break

		// Cookie Button
		case 1:
			break } }


/* Purpose: Gets called when a menu entry in the blocked sites or whitelisted domains popup list is clicked.
 *          Asks the user whether she wants to add/remove it to/from the whitelist and acts accordingly
 */
const onChangeWhitelistEntry = function(event) {
	if (event.target.value) {
		// This mode removes a domain from the whitelist. Only left button mouse clicks are processed here
		if (event.button == 0) {
			var domainList  = event.target.getUserData('domainList')
			var settingPath = event.target.getUserData('settingPath')

			// Find and remove the clicked domain name from the whitelist
			for (var index in domainList) {
				if (domainList[index] == event.target.label) {
					domainList.splice(index, 1)
					break } }

			// Make a question
			var queryText = 'Remove domain <' + event.target.label + '> from '
			if (event.target.value == '<embeds>') {
				queryText   = queryText + 'the global embed whitelist?' }
			else {
				queryText   = queryText + '<' + event.target.value + '>\'s whitelist?' }

			// For convenience, automatically reloads the tab's content after rewriting the whitelist
			if (promptService.confirm(null, 'Confirm your choice', queryText)) {
				if (domainList.length == 0 || domainList[0] == '') {
					preferenceManager.clearUserPref(settingPath)
					if (preferenceManager.getPrefType(settingPath) == PREF_STRING) {
						preferenceManager.setCharPref(settingPath, '') } }
				else {
					preferenceManager.setCharPref(settingPath, domainList.join(',')) }
				gBrowser.selectedBrowser.contentWindow.location.reload() } } }
	else {
		// This mode adds a domain to the whitelist
		var currentDomain = event.target.getUserData('sourceDomain')
		var domainToAdd   = event.target.getUserData('destinationDomain')

		// Make a question
		var settingPath, queryText = 'Add domain <' + domainToAdd + '> to '
		if (event.button == 2) {
			var responsibleTag = event.target.getUserData('responsibleTag')
			if (responsibleTag in settings.embedTagClass) {
				settingPath = 'extensions.minimalist.embed'
				queryText   = queryText + 'the global embed whitelist?' }
			else {
				return } }
		else {
			settingPath = 'extensions.minimalist.whitelist.' + currentDomain
			queryText   = queryText + '<' + currentDomain + '>\'s whitelist?' }

		// Append the new domain to the existing list of domains - or create a new list if it doesn't exist
		var currentList = ''
		if (preferenceManager.getPrefType(settingPath) == PREF_STRING) {
			currentList = currentList + preferenceManager.getCharPref(settingPath)
			if (currentList != '') {
				currentList = currentList + ',' } }
		currentList = currentList + domainToAdd

		// For convenience, automatically reloads the tab's content after adding the whitelist entry
		if (promptService.confirm(null, 'Confirm your choice', queryText)) {
			preferenceManager.setCharPref(settingPath, currentList)
			gBrowser.selectedBrowser.contentWindow.location.reload() } } }


/* Purpose: Gets called when the user clicks a menu item for changing a setting
 */
const onChangeSetting = function(event) {
	if (event.target.getAttribute('type')) {
		// This mode simply toggles skipRefresh. The menu item is toggled automatically
		var settingPath = 'extensions.minimalist.skipRefresh'
		var newValue    = !preferenceManager.getBoolPref(settingPath)

		preferenceManager.setBoolPref(settingPath, newValue) } }


/* Purpose: These two functions delete all blocked sites entries for a tab when they become invalid
 */
const onLocationChange = function(browser) {
	delete settings.blockedSites[browser.contentWindow.location.href] }

const onTabClosed = function(event) {
	delete settings.blockedSites[event.target.linkedBrowser.contentWindow.location.href] }


/* Purpose: Gets called once the XUL overlay is loaded. It sets up everything we need for our statusbar panel
 */
const onOverlayLoaded = function() {
	// Track every location change in a tab so we can clear its current blocked sites list
	gBrowser.addTabsProgressListener({
		onLocationChange: onLocationChange,
		   onStateChange: function() {},
		onProgressChange: function() {},
		  onStatusChange: function() {},
		onSecurityChange: function() {}})

	// Closing the tab also makes its blocked sites list invalid
	gBrowser.tabContainer.addEventListener('TabClose', onTabClosed, false)

	// Set initial values for statusbar panel
	onButtonClicked(document.getElementById('minimalist-filterLevel'), 0, 4) }

window.addEventListener('load', onOverlayLoaded, false)
