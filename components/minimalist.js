function NSGetModule() {
	// Providing shortcuts for many otherwise lengthy XPCOM interfaces and constants
	const ACCEPT               = Components.interfaces.nsIContentPolicy.ACCEPT
	const REJECT               = Components.interfaces.nsIContentPolicy.REJECT_SERVER
	const nsIAnchorElement     = Components.interfaces.nsIDOMHTMLAnchorElement
	const nsIConsoleService    = Components.interfaces.nsIConsoleService
	const nsIContentPolicy     = Components.interfaces.nsIContentPolicy
	const nsIEmbedElement      = Components.interfaces.nsIDOMHTMLEmbedElement
	const nsIFrameElement      = Components.interfaces.nsIDOMHTMLFrameElement
	const nsIHttpChannel       = Components.interfaces.nsIHttpChannel
	const nsIImageElement      = Components.interfaces.nsIDOMHTMLImageElement
	const nsILinkElement       = Components.interfaces.nsIDOMHTMLLinkElement
	const nsIObjectElement     = Components.interfaces.nsIDOMHTMLObjectElement
	const nsIObserver          = Components.interfaces.nsIObserver
	const nsIObserverService   = Components.interfaces.nsIObserverService
	const nsIPrefBranch        = Components.interfaces.nsIPrefBranch
	const nsIPrefBranch2       = Components.interfaces.nsIPrefBranch2
	const nsIURI               = Components.interfaces.nsIURI
	const nsIURL               = Components.interfaces.nsIURL
	const nsIWindow            = Components.interfaces.nsIDOMWindow
	const nsIWindowWatcher     = Components.interfaces.nsIWindowWatcher
	const nsIXULElement        = Components.interfaces.nsIDOMXULElement

	// Generic constants
	const allowedSchemes       = {    'about': true,   'chrome': true,    'file': true,
	                               'moz-icon': true, 'resource': true, 'wyciwyg': true}
	const tagsToClear          = ['Age', 'Cache-Control', 'Date', 'ETag', 'Last-Modified', 'Pragma', 'Vary']
	const embedTagClass        = {'EMBED': true, 'OBJECT': true, 'VIDEO': true}
	const urlParser            = Components.classes['@mozilla.org/network/standard-url;1'].getService(nsIURL)
	const preferenceManager    = Components.classes['@mozilla.org/preferences-service;1'].getService(nsIPrefBranch)
	const observerService      = Components.classes['@mozilla.org/observer-service;1'].getService(nsIObserverService)

	// Various regular expressions. They are automatically compiled once on startup due to the literal notation
	const isHTMLDocument       = /^(?:text\/html|application\/xhtml\+xml)/
	const mustBeCached         = /^(?:text(?!\/html)|image|multipart)\/|^application\/(?:x-)?(?:javascript|json)/
	const extractDomainName    = /([^.]+)\.(?:(?:net?|com?|or|ac)\.)?[^\d.]+$/
	const isAnImageFileName    = /\.(?:jpe?g|png|gif|bmp)$/i
	const advertBlacklist      = /(?:^|[\/.])ad(?:click|vert(is(?:es?|ings?))?|s(?:erv(?:er)?|pot))?s?\d?(?:[\/.]|$)/i

	// These variables hold settings used throughout this extension
	var embedExceptions        = {}
	var whiteList              = {}
	var sourceDomain           = null
	var destinationDomain      = null
	var linkNode               = null
	var isLinkedImageNode      = false

	// Having an own constant for every filter level enables us to find out which level blocked a load request
	const REJECT_FLAG          = 4
	const REJECT_L1            = REJECT_FLAG | 1
	const REJECT_L2            = REJECT_FLAG | 2
	const REJECT_L3            = REJECT_FLAG | 3

	// Initialize map filter. Javascript unfortunately has no literal syntax for this task
	var mapFilterResult        = {};
	mapFilterResult[   ACCEPT] = ACCEPT
	mapFilterResult[REJECT_L1] = REJECT
	mapFilterResult[REJECT_L2] = REJECT
	mapFilterResult[REJECT_L3] = REJECT


	/* Purpose: Extracts and returns only the domain name from an URL
	 */
	const getDomainName = function(URL) {
		urlParser.spec = URL
		return (extractDomainName.exec(urlParser.host) || {1: urlParser.host})[1].toLowerCase() }


	/* Purpose: Replaces the current linked image with a message that indicates which filter level blocked it
	 */
	const disableLinkedImage = function(level) {
		if (isLinkedImageNode) {
			var blockedLink         = linkNode.ownerDocument.createElement('a')
			blockedLink.href        = linkNode.href
			blockedLink.textContent = '[Blocked L' + level + ']'
			linkNode.parentNode.replaceChild(blockedLink, linkNode)
			linkNode                = blockedLink
			isLinkedImageNode       = true } }


	/* Purpose: Logs all blocked requests into the error console
	 */
	const logBlockedRequests = function(unused, destinationURL, sourceURL, triggeringNode) {
		var filterResult = filterLoadRequest(destinationURL, sourceURL, triggeringNode)

		if (filterResult & REJECT_FLAG && triggeringNode && triggeringNode.ownerDocument) {
			var blockedSites = Minimalist.prototype.blockedSites
			var tabWindow    = triggeringNode.ownerDocument.defaultView

			// Dereferencing the document's window until we reach the topmost window i.e. the browser's window
			while (tabWindow != tabWindow.parent) {
				tabWindow = tabWindow.parent }
			tabWindow = tabWindow.location.href

			// Resources blocked on level 2 or 3 need their image's URL as destination URL
			if (filterResult == REJECT_L2 || filterResult == REJECT_L3 && isLinkedImageNode) {
				urlParser.spec    = linkNode.href
				destinationDomain = getDomainName(linkNode.href) }
			else {
				urlParser.spec = destinationURL.spec }

			// Adds detailed info about the blocked resource which can be retrieved by the statusbar button
			if (!(tabWindow in blockedSites)) {
				blockedSites[tabWindow]                    = {} }
			if (!(urlParser.prePath in blockedSites[tabWindow])) {
				blockedSites[tabWindow][urlParser.prePath] = {} }

			blockedSites[tabWindow][urlParser.prePath][urlParser.filePath] = {
				     'sourceDomain': sourceDomain,
				'destinationDomain': destinationDomain,
				      'filterLevel': filterResult ^ REJECT_FLAG,
				   'responsibleTag': triggeringNode.wrappedJSObject.tagName } }

		return mapFilterResult[filterResult] }


	/* Purpose: Has three different filter levels that try to prevent advertisement and annoying scripts from loading.
	 *          resource:// and chrome:// URLs and XUL elements are always granted to load, so the UI doesn't break
	 */
	const filterLoadRequest = function(destinationURL, sourceURL, triggeringNode) {
		if (         Minimalist.prototype.level < 1 || triggeringNode == null ||     !sourceURL ||
		    triggeringNode instanceof nsIXULElement || triggeringNode instanceof nsILinkElement ||
		         sourceURL.scheme in allowedSchemes || destinationURL.scheme in allowedSchemes) {
			return ACCEPT }

		// When checking whether a resource is external, we always only compare the domain names
		    sourceDomain      = getDomainName(sourceURL.spec)
		    destinationDomain = getDomainName(destinationURL.spec)
		var whiteListDomains  = whiteList[sourceDomain] || {}

		// Filter level 1: Embed and object tags have an own whitelist that's valid for any source domain
		if (triggeringNode.tagName in embedTagClass && destinationDomain in embedExceptions) {
			return ACCEPT }

		// Determine wheter the triggering node is an image linked to a valid target
		linkNode          = triggeringNode.parentNode
		isLinkedImageNode = triggeringNode instanceof nsIImageElement && linkNode instanceof nsIAnchorElement &&
		                    linkNode.href != '' && linkNode.href.substr(0, 11) != 'javascript:'

		// Filter level 2: Denies to load if the triggering node is an image,
		// but does link to some other external resource, that is not an image
		if (Minimalist.prototype.level > 1 && isLinkedImageNode) {
			if (isAnImageFileName.test(linkNode.href)) {
				return ACCEPT }

			var linkDomain = getDomainName(linkNode.href)
			if (linkDomain != sourceDomain && !(linkDomain in whiteListDomains)) {
				disableLinkedImage(2)
				return REJECT_L2 } }

		// Filter level 1: Denies to load any external resource that aren't on this domain's whiteList
		if (sourceDomain != destinationDomain && !(destinationDomain in whiteListDomains)) {
			if (triggeringNode instanceof nsIImageElement) {
				linkNode             = triggeringNode.ownerDocument.createElement('a')
				linkNode.href        = triggeringNode.src
				linkNode.textContent = '[Blocked L1]'
				triggeringNode.parentNode.replaceChild(linkNode, triggeringNode) }
			return REJECT_L1 }

		// Filter level 3: This is the last measure. Checks the destination URL and link URL for blacklisted tokens
		if (Minimalist.prototype.level > 2 && (
		   (               advertBlacklist.test(destinationURL.spec) && !(destinationDomain in whiteListDomains)) ||
		   (isLinkedImageNode && advertBlacklist.test(linkNode.href) && !(       linkDomain in whiteListDomains)))) {
			disableLinkedImage(3)
			return REJECT_L3 }

		// Fall-through case
		return ACCEPT }


	/* Purpose: Wrapper for httpChannel.getRequestHeader that doesn't crash
	 */
	const getRequestHeader = function(httpChannel, tagName) {
		try       { return httpChannel.getRequestHeader(tagName) }
		catch (e) { return false } }


	/* Purpose: Wrapper for httpChannel.getResponseHeader that doesn't crash
	 */
	const getResponseHeader = function(httpChannel, tagName) {
		try       { return httpChannel.getResponseHeader(tagName) }
		catch (e) { return false } }


	/* Purpose: Removes all known HTTP tags related to caching and user tracking;
	 *          inserts it an Expiry tag afterwards to enforce our own caching rules
	 *   Notes: httpChannel is of type nsISupports and must be casted to nsIHttpChannel
	 */
	const filterIncomingHTTP = function(httpChannel) {
		var httpChannel = httpChannel.QueryInterface(nsIHttpChannel)

		for each (var tagName in tagsToClear) {
			httpChannel.setResponseHeader(tagName, '', false) }

		// This is the fall-through case
		var expiryDate = 'Thu, 01 Jan 1970 00:00:00 GMT'

		// Content with MIME type text/*, image/* or multipart/* will be cached forever if known to be smaller than 2 MiB
		var contentType = getResponseHeader(httpChannel, 'Content-Type') || ''
		if ((contentLength = getResponseHeader(httpChannel, 'Content-Length')) &&
		     parseInt(contentLength) <= (2*1024*1024) && mustBeCached.test(contentType)) {
			expiryDate = 'Sun, 07 Feb 2106 06:28:15 GMT' }

		httpChannel.setResponseHeader('Expires', expiryDate, false)
		httpChannel.setResponseHeader('Last-Modified', new Date().toGMTString(), false) }


	/* Purpose: Overwrites the User-Agent and Referer HTTP tags to archive better privacy
	 *   Notes: httpChannel is of type nsISupports and must be casted to nsIHttpChannel
	 */
	const filterOutgoingHTTP = function(httpChannel) {
		var httpChannel = httpChannel.QueryInterface(nsIHttpChannel)

		// If the Referer's domain isn't the same as the target domain, make it so, but leave it untouched if it is
		var referer = getRequestHeader(httpChannel, 'Referer')
		var domain  = getRequestHeader(httpChannel, 'Host')

		if (referer && domain) {
			var refererDomain = getDomainName(referer)
			var currentDomain = getDomainName('http://' + domain)

			if (refererDomain != currentDomain) {
				httpChannel.setRequestHeader('Referer', 'http://' + domain, false) } }

		// This is the User-Agent sent by the most recent official build (as of now) when running on Windows XP
		httpChannel.setRequestHeader('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.7) '
		                                         + 'Gecko/20091221 Firefox/3.5.7', false) }


	/* Purpose: Reads the new value pointed to by settingPath into a local variable
	 */
	const readSetting = function(settingPath) {
		var pathTokens = settingPath.split('.')

		switch (pathTokens[2]) {
			case 'level':
				Minimalist.prototype.level       = preferenceManager.getIntPref(settingPath)
				break

			case 'skipRefresh':
				Minimalist.prototype.skipRefresh = preferenceManager.getBoolPref(settingPath)
				break

			case 'embed':
				var destinationDomains = preferenceManager.getCharPref(settingPath).split(',')
				embedExceptions = {}

				for each (var destinationDomain in destinationDomains) {
					embedExceptions[destinationDomain] = true }
				break

			case 'whitelist':
				var sourceDomain       = pathTokens[3]
				var destinationDomains = preferenceManager.getCharPref(settingPath).split(',')
				whiteList[sourceDomain] = {}

				for each (var destinationDomain in destinationDomains) {
					whiteList[sourceDomain][destinationDomain] = true }
				break } }


	/* Purpose: The ObserverService notifies us of all events we've subscribed to by calling this function
	 */
	const observerCallback = function(triggeringObject, eventName, eventData) {
		switch (eventName) {
			case 'http-on-examine-response':
				filterIncomingHTTP(triggeringObject)
				break

			case 'http-on-modify-request':
				filterOutgoingHTTP(triggeringObject)
				break

			case 'nsPref:changed':
				readSetting(eventData)
				break } }


	// Prepare for XPCOM registration
	Components.utils.import('resource://gre/modules/XPCOMUtils.jsm')
	const Minimalist = function() { this.wrappedJSObject = this }
	Minimalist.prototype = {
	   classDescription: 'Minimalist',
	         contractID: '@zelgadis.jp/minimalist;1',
	            classID: Components.ID('{124b1de7-bd8a-4a41-a04a-9d40070a0b3a}'),
	     QueryInterface: XPCOMUtils.generateQI([nsIContentPolicy]),
	  _xpcom_categories: [{category: 'content-policy'}],
	         shouldLoad: logBlockedRequests,
	      shouldProcess: function() { return ACCEPT },
	      getDomainName: getDomainName,
	      embedTagClass: embedTagClass }

	// Adding the nsIPrefBranch2 interface gives us access to the getChildList method
	var registrar = { observe: observerCallback }
	preferenceManager.QueryInterface(nsIPrefBranch2)
	preferenceManager.addObserver('extensions.minimalist.', registrar, false)
	observerService.addObserver(registrar, 'http-on-examine-response', false)
	observerService.addObserver(registrar, 'http-on-modify-request',   false)

	// Populate all settings with their initial values so they can be accessed right away
	var whiteListEntries = preferenceManager.getChildList('extensions.minimalist.whitelist', {})
	for each (whiteListEntry in whiteListEntries) { readSetting(whiteListEntry) }
	readSetting('extensions.minimalist.level')
	readSetting('extensions.minimalist.skipRefresh')
	readSetting('extensions.minimalist.embed')
	Minimalist.prototype.blockedSites = {}

	return XPCOMUtils.generateModule([Minimalist]) }
