#!/bin/sh
PATTERN='s/^.*>\(.*\)<.*$/\1/g'
VER=`cat install.rdf | grep '<em:version>' | sed $PATTERN`
FILES='components content defaults style install.rdf chrome.manifest'

zip -r minimalist_$VER.xpi $FILES
